# \<naam van template minus gitlab-ci.yml\>
`versie <version>`

\<korte-beschrijving\>

## Handleiding
\<uitleg over gebruikt, bijv. includes\>

### Randvoorwaarden
\<randvoorwaarden voor gebruik, bijv. stages\>

### Parameters

| Parameter 	| Beschrijving                     	| Default waarde 	|
|-----------	|----------------------------------	|----------------	|
| `<parameter-1>`         	| \<beschrijving\>       	| \<`eventuele default waarde` of -\>              	|
| `<parameter-2>`         	| \<beschrijving\>       	| \<`eventuele default waarde` of -\>              	|

### Voorbeelden
```yaml
include:
- project: 'EDSN/itf-templates/gitlab-ci-templates'
  ref: v22.1
  file:
  - '<filenaam>'

``` 

## Changelog

### \<version n+1\>
\<wijzigingen voor n+1\>

### \<version n\>
\<wijzigingen voor n\>

## Upgrade guide

### \<version n\> -\> \<version n+1\>
\<stappen voor upgrade\>
